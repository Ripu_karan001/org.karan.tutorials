
package org.karan.tutorials.getStudentsInformation.bo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "Student"
})
public class StudentDetails {

    @JsonProperty("Student")
    private Student student;

    @JsonProperty("Student")
    public Student getStudent() {
        return student;
    }

    @JsonProperty("Student")
    public void setStudent(Student student) {
        this.student = student;
    }

}
