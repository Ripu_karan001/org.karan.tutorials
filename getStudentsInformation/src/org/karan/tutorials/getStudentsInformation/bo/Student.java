
package org.karan.tutorials.getStudentsInformation.bo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "studentID",
    "Name",
    "Dept "
})
public class Student {

    @JsonProperty("studentID")
    private String studentID;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Dept ")
    private String dept;

    @JsonProperty("studentID")
    public String getStudentID() {
        return studentID;
    }

    @JsonProperty("studentID")
    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Dept ")
    public String getDept() {
        return dept;
    }

    @JsonProperty("Dept ")
    public void setDept(String dept) {
        this.dept = dept;
    }

}
